﻿using System.Linq;
using ShopCheckout.Data.Models;

namespace ShopCheckout.Data.Repositories
{
    public class ProductsRepository
    {
        public Product Get(int id)
        {
            Product product;

            using (var dbContext = new ShopCheckoutDbContext())
            {
                product = dbContext.ProductsDbSet.Single(p => p.Id == id);
            }

            return product;
        }

        public void Add(Product product)
        {
            using (var dbContext = new ShopCheckoutDbContext())
            {
                dbContext.ProductsDbSet.Add(product);
                dbContext.SaveChanges();
            }
        }
    }
}
