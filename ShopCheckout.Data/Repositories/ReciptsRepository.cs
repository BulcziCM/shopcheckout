﻿using ShopCheckout.Data.Models;

namespace ShopCheckout.Data.Repositories
{
    public class ReciptsRepository
    {
        public void Add(Recipt recipt)
        {
            using (var dbContext = new ShopCheckoutDbContext())
            {
                dbContext.ReciptDbSet.Add(recipt);
                dbContext.SaveChanges();
            }
        }
    }
}
