﻿namespace ShopCheckout.Data.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double PricePerUnit { get; set; }
        public bool IsPricedByWeights { get; set; }
    }
}
