﻿using System;
using System.Collections.Generic;

namespace ShopCheckout.Data.Models
{
    public class Recipt
    {
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public List<Product> Products { get; set; }
    }
}