﻿using System.Configuration;
using System.Data.Entity;
using ShopCheckout.Data.Models;

namespace ShopCheckout.Data
{
    internal class ShopCheckoutDbContext : DbContext
    {
        public DbSet<Product> ProductsDbSet { get; set; }
        public DbSet<Recipt> ReciptDbSet { get; set; }

        public ShopCheckoutDbContext() : base(GetConnectionString())
        { }

        public static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["ShopCheckoutDb"].ConnectionString;
        }
    }
}