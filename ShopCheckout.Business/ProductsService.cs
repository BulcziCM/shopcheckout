﻿using System;
using ShopCheckout.Data.Models;
using ShopCheckout.Data.Repositories;

namespace ShopCheckout.Business
{
    public class ProductsService
    {
        public bool CheckIfProductExists(int id)
        {
            var productRepository = new ProductsRepository();

            try
            {
                productRepository.Get(id);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool CheckIfPricedByWeight(int id)
        {
            var productRepository = new ProductsRepository();
            return productRepository.Get(id).IsPricedByWeights;
        }

        public void AddDummyProducts()
        {
            var productRepository = new ProductsRepository();

            productRepository.Add(new Product
            {
                Name = "Banana",
                PricePerUnit = 4.5d,
                IsPricedByWeights = true
            });

            productRepository.Add(new Product
            {
                Name = "Bread",
                PricePerUnit = 3.25,
                IsPricedByWeights = false
            });

            productRepository.Add(new Product
            {
                Name = "Beer",
                PricePerUnit = 5.0d,
                IsPricedByWeights = false
            });

            productRepository.Add(new Product
            {
                Name = "Tomato",
                PricePerUnit = 3.0d,
                IsPricedByWeights = true
            });

            productRepository.Add(new Product
            {
                Name = "Water",
                PricePerUnit = 2.2d,
                IsPricedByWeights = false
            });
        }
    }
}
