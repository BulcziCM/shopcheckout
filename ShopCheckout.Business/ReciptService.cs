﻿using System;
using System.Collections.Generic;
using ShopCheckout.Business.Dtos;
using ShopCheckout.Data.Models;
using ShopCheckout.Data.Repositories;

namespace ShopCheckout.Business
{
    public class ReciptService
    {
        public double StoreAndSummarizeRecipt(ReciptDto reciptDto)
        {
            var totalCost = 0.0d;
            var productsRepository = new ProductsRepository();
            var reciptRepository = new ReciptsRepository();
            var recipt = new Recipt
            {
                CreationTime = DateTime.Now,
                Products = new List<Product>()
            };

            foreach (var productCheckout in reciptDto.Products)
            {
                var product = productsRepository.Get(productCheckout.ProductId);
                totalCost += product.PricePerUnit * productCheckout.Amount;

                recipt.Products.Add(product);
            }

            reciptRepository.Add(recipt);
            return totalCost;
        }
    }
}
