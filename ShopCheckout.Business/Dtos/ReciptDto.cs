﻿using System.Collections.Generic;

namespace ShopCheckout.Business.Dtos
{
    public class ReciptDto
    {
        public List<ProductCheckoutDto> Products = new List<ProductCheckoutDto>();
    }
}