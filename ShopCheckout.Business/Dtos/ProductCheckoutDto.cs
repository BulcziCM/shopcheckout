﻿namespace ShopCheckout.Business.Dtos
{
    public class ProductCheckoutDto
    {
        public int ProductId;
        public double Amount;
    }
}
