﻿using System;
using ShopCheckout.Business;
using ShopCheckout.Business.Dtos;

namespace ShopCheckout.Cli
{
    internal class Program
    {
        private static void Main()
        {
            new Program().Run();
        }

        private void Run()
        {
            //AddDummyProducts();
            while (true)
            {
                Checkout();
            }
        }

        private void AddDummyProducts()
        {
            var productsService = new ProductsService();
            productsService.AddDummyProducts();
        }

        private void Checkout()
        {
            var exit = false;
            var recipt = new ReciptDto();
            var productsService = new ProductsService();

            while (!exit)
            {
                Console.Write("Provide product code: ");
                var productCheckoutDto = new ProductCheckoutDto();
                productCheckoutDto.ProductId = int.Parse(Console.ReadLine());

                if (productCheckoutDto.ProductId == -1)
                {
                    break;
                }

                if (!productsService.CheckIfProductExists(productCheckoutDto.ProductId))
                {
                    Console.WriteLine("There is no such product");
                    continue;
                }

                if (productsService.CheckIfPricedByWeight(productCheckoutDto.ProductId))
                {
                    Console.Write("Provice weight: ");
                    productCheckoutDto.Amount = double.Parse(Console.ReadLine());
                }
                else
                {
                    Console.Write("Provice amount: ");
                    productCheckoutDto.Amount = int.Parse(Console.ReadLine());
                }

                recipt.Products.Add(productCheckoutDto);
            }

            Console.WriteLine("Summarizing...");

            var totalCost = new ReciptService().StoreAndSummarizeRecipt(recipt);
            Console.WriteLine("Total cost: " + totalCost);
        }
    }
}